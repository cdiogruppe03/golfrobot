package behaviors;

import data.RobotState;
import lejos.hardware.Sound;
import lejos.robotics.subsumption.Behavior;

public class BehaviorPlaceHolderNoMoreBalls implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return robotState.isNoMoreBalls();
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR PLACEHOLDER NO MORE BALLS ***");
		System.exit(0);
	}

	@Override
	public void suppress() {
		suppressed = true;
	}

}
