package behaviors;

import data.RobotState;
import lejos.robotics.subsumption.Behavior;

public class BehaviorWaitForGoalLocation implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	
	@Override
	public boolean takeControl() {
		return robotState.isUpdateGoalLocation() && robotState.isHaveLinemap();
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR WAIT FOR GOAL LOCATION ***");
		
		while(!suppressed && robotState.isUpdateGoalLocation())
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
	
	
}
