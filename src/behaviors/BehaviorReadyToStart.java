package behaviors;

import utility.EventTimerListener;
import data.RobotState;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.CompassPilot;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;
import lejos.utility.Timer;

public class BehaviorReadyToStart implements Behavior {
	boolean supressed = true;
	
	RobotState robotState = RobotState.getInstance();
	@Override
	public boolean takeControl() {
		supressed = false;
		return !robotState.isReadyToStart() 
				&& robotState.isHaveLinemap()
				&& robotState.isReadyToMoveToDestination()
				&& robotState.isPoseUptoDate();
	}

	@Override
	public void action() {
//		((CompassPilot) robotState.getPilot()).calibrate();
		
		printAsciiArt();
		SampleProvider sampleProvider = robotState.getIrSensor().getSeekMode();
		float [] vals = new float [8];
		
		Sound.beepSequenceUp();
		sampleProvider.fetchSample(vals, 0);
		int bearing = (int) vals[0];
		while(bearing==0) {
			Delay.msDelay(50);
			sampleProvider.fetchSample(vals, 0);
			bearing = (int) vals[0];
		}
		
		robotState.setReadyToStart(true);
		robotState.setTimeStampStart(System.currentTimeMillis());
		robotState.setEventTimer(new Timer(7*60*1000, new EventTimerListener()));
	}

	@Override
	public void suppress() {
		supressed = true;
	}
	
	private void printAsciiArt() {
		
		StringBuilder art = new StringBuilder();
		art.append(" _____  _____  ______  _____ _____            _   ___     __  ____  _    _ _______ _______ ____  _   _   _______ ____  \n");
		art.append("|  __ \\|  __ \\|  ____|/ ____/ ____|     /\\   | \\ | \\ \\   / / |  _ \\| |  | |__   __|__   __/ __ \\| \\ | | |__   __/ __ \\ \n");
		art.append("| |__) | |__) | |__  | (___| (___      /  \\  |  \\| |\\ \\_/ /  | |_) | |  | |  | |     | | | |  | |  \\| |    | | | |  | |\n");
		art.append("|  ___/|  _  /|  __|  \\___ \\\\___ \\    / /\\ \\ | . ` | \\   /   |  _ <| |  | |  | |     | | | |  | | . ` |    | | | |  | |\n");
		art.append("| |    | | \\ \\| |____ ____) |___) |  / ____ \\| |\\  |  | |    | |_) | |__| |  | |     | | | |__| | |\\  |    | | | |__| |\n");
		art.append("|_|    |_|  \\_\\______|_____/_____/  /_/    \\_\\_| \\_|  |_|    |____/ \\____/   |_|     |_|  \\____/|_| \\_|    |_|  \\____/ \n");
		art.append("\n");
		art.append("  _____ _______       _____ _______ \n");
		art.append(" / ____|__   __|/\\   |  __ \\__   __|\n");
		art.append("| (___    | |  /  \\  | |__) | | |   \n");
		art.append(" \\___ \\   | | / /\\ \\ |  _  /  | |   \n");
		art.append(" ____) |  | |/ ____ \\| | \\ \\  | |   \n");
		art.append("|_____/   |_/_/    \\_\\_|  \\_\\ |_|   \n");
		System.out.println(art.toString());
		                                     
	}

}
