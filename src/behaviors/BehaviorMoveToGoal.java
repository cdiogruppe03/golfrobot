package behaviors;

import data.RobotState;
import lejos.hardware.Sound;
import lejos.robotics.navigation.DestinationUnreachableException;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.subsumption.Behavior;
import lejos.robotics.RangeFinderAdapter;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.CompassPilot;


@SuppressWarnings("deprecation")
public class BehaviorMoveToGoal implements Behavior {
	private boolean suppressed = true;
	private RobotState robotState = RobotState.getInstance();
	
	DifferentialPilot pilot = robotState.getPilot();
	RangeFinderAdapter rf = robotState.getRf();
	
	RegulatedMotor leftMotor = robotState.getMotorB();
	RegulatedMotor rightMotor = robotState.getMotorC();
	
	private double distance = 200; // Object to sensor

	@Override
	public boolean takeControl() {
		return (robotState.isTimeIsUp()
				&& robotState.isMoveToGoal())
				||(robotState.isPoseFailure() 
				&& robotState.isMoveToGoal())
				|| (robotState.isConnectionFailure()
				&& robotState.isMoveToGoal())
				|| (robotState.isNoMoreBalls() 
				&& !robotState.isDeliverBalls() 
				&& robotState.isPoseUptoDate()
				);
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR MOVE TO GOAL ***");

		if(robotState.isDebug()) {

			System.out.println(robotState);	
		}
		
		try {
			Path path = robotState.getPathFinder().findRoute(
					robotState.getNavigator().getPoseProvider().getPose(),
					robotState.getGoalLocation()
					);

			robotState.getNavigator().followPath(path);
			
			while(robotState.getNavigator().isMoving() && !suppressed)
				Thread.yield(); 
			
			Waypoint goalInBorder = new Waypoint(robotState.getGoalLocation().getX()+25, robotState.getGoalLocation().getY());
			
			
			System.out.println("Robot pose: " + robotState.getNavigator().getPoseProvider().getPose().toString());
			System.out.println("Goal: " + robotState.getGoalLocation().toString());
			System.out.println("Goal border: " + goalInBorder.toString());
			
			robotState.correctHeading(robotState.getNavigator().getPoseProvider().getPose(),
					goalInBorder,
					0);
			
			System.out.println("Heading after corr: " + robotState.getNavigator().getPoseProvider().getPose().toString());
			
			setMotorSpeed(130); // Is this appropriate?
			
			setMotorDirection(true); // FORWARD --->
			
			
			// Checking that we do not hit the edge :(
			double range = rf.getRange();
			while(range > distance) {
				if(robotState.isDebug()) {
					System.out.println("Distance: " + range);
				}
				range = rf.getRange();	
			}
			if(robotState.isDebug()) {
				System.out.println("Distance: " + range);
			}
			
			stopMotors();
			
			
			robotState.setDeliverBalls(true);
			robotState.setMoveToGoal(false);
		} catch (DestinationUnreachableException e) {
			System.out.println("Destination unreachable");
			Sound.buzz();
			Sound.buzz();
			Sound.buzz();
		} catch(NullPointerException e) {
			/* Why? Ask god */
			suppressed = true;
			robotState.stateDefault();
		}
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
	
	private void setMotorDirection(boolean forward) {
		if(forward) {
			leftMotor.forward();
			rightMotor.forward();
		} else {
			rightMotor.backward();
			leftMotor.backward();
		}
	}
	
	private void setMotorSpeed(int speed) {
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(speed);
	}
	
	private void stopMotors() {
		leftMotor.stop(true);
		rightMotor.stop(true);		
	}

}
