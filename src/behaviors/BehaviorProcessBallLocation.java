package behaviors;

import data.RobotState;
import lejos.hardware.Sound;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.subsumption.Behavior;

public class BehaviorProcessBallLocation implements Behavior{
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	private boolean processingBall = false;
	private double currentX, currentY;

	@Override
	public boolean takeControl() {
		return (robotState.isBallLocationReceived() 
				&& robotState.isPoseUptoDate() 
				&& !robotState.isNoMoreBalls()) 
				|| processingBall;
	}

	@Override
	public void action() {
		suppressed = false;
		robotState.setBallInDangerZone(false);
		if(!processingBall)
			processingBall = true;
			System.out.println("*** BEHAVIOR PROCESS BALL ***");
			if(robotState.isDebug()) {
				System.out.println(robotState);	
			}			

			new Thread(new BallLocationUpdater()).start();
		
		while(processingBall && !suppressed)
			Thread.yield();
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
	
	private class BallLocationUpdater implements Runnable{
		
		@Override
		public void run() {
			try {
				
				// Beregner et ghost punkt, hvis vi er i en danger zone
				determinePointInBorderDangerZone();
				if(robotState.isCrossReceived()) {
					determinePointInCrossDangerZone();
				}
				
				
				if(robotState.isBallInDangerZone()) {
					robotState.setCurrentDestination(new Waypoint(currentX, currentY, determineHeading()));
				} else {
					robotState.setCurrentDestination(robotState.getBallLocation());
				}
					
				robotState.setReadyToMoveToDestination(true);
				processingBall = false;
				
			} catch(NullPointerException e) {
				System.out.println("**** NULLPOINTER EXCEPTION ****");
				System.out.println(robotState);
			}
		}
		
		/**
		 * Beregning af nyt ghost punkt, når en bold er placeret 
		 * tæt på banden, således at vi ikke støder ind i banden.
		 */
		private void determinePointInBorderDangerZone() {
			double noseLength = robotState.getROBOT_NOSE_LENGTH();
			Waypoint ballLocation = robotState.getBallLocation();
			double dangerZone = robotState.getDangerZone();
			
			currentX = ballLocation.getX();
			currentY = ballLocation.getY();
			
			
			// Periferi omkring banen, som sætter ghost waypoint, når bolden er tæt på banden
			if(ballLocation.getX() < dangerZone) {
				robotState.setBallInDangerZone(true);
				currentX = currentX + noseLength + 5;				
			} else if(ballLocation.x > robotState.getMax_X()-dangerZone) {
				robotState.setBallInDangerZone(true);
				currentX = robotState.getMax_X() - noseLength - 5;
			}
			
			if(ballLocation.y < dangerZone) {
				robotState.setBallInDangerZone(true);
				currentY = currentY + noseLength + 5;

			} else if(ballLocation.y > robotState.getMax_Y()-dangerZone) {
				robotState.setBallInDangerZone(true);
				currentY = robotState.getMax_Y() - noseLength - 5; 
			}
			
			if(Math.abs(ballLocation.getX()-currentX) > 1 &&
					Math.abs(ballLocation.getY()-currentY) > 1) {
				robotState.setBallInCorner(true);
			}
		}
	
		
		
		/***
		 * Beregning af ghost waypoint, når en bold ligger inden for en
		 * bestemt størrelse cirkel rundt om krydset. 
		 * 
		 * Radius til cirklen er defineret i robotState.crossDangerZone
		 */
		private void determinePointInCrossDangerZone() {
			// Beregner distancen mellem centrum af krydset (defineret i robotState) og bolden's (x,y) koordinater.
			double ballDistanceToCross = Math.sqrt(Math.pow((robotState.getCrossCenter().getX()-robotState.getBallLocation().getX()), 2)
					+(Math.pow((robotState.getCrossCenter().getY()-robotState.getBallLocation().getY()), 2)));
			
			System.out.println("ballDistanceToCorss: " + ballDistanceToCross);
			if(ballDistanceToCross < 25) {
				//Bolden ligger inden for dangerzone cirklen.
				robotState.setBallInDangerZone(true);
				robotState.setBallInCross(true);
				
				//Udregning af ghost position - Gennemgået i rapport
				double radius = 30.0;

				double theta = Math.atan2(robotState.getBallLocation().getY()-robotState.getCrossCenter().getY(),
						robotState.getBallLocation().getX()-robotState.getCrossCenter().getX());

				currentX = (radius * Math.cos(theta)) + robotState.getCrossCenter().getX();
				currentY = (radius * Math.sin(theta)) + robotState.getCrossCenter().getY();
			}
		}
		
		/**
		 * Beregner den heading robotten skal have, når vi vi slutter på et punkt.
		 * F.eks. skal denne heading, på et frit punkt, være den heading robotten har, når
		 * vi kører hen til robotten, altså så vi ikke bruger tid på at rotere robotten unødigt.
		 * 
		 * Når vi f.eks. skal ind til et hjørne, beregner vi headingen vha af ballpoint og 
		 * punktet i hjørnet.
		 */
		private double determineHeading() {
			double x1, y1, x2, y2;
			if(robotState.isBallInDangerZone()) {
				x1 = currentX;
				y1 = currentY;
			} else {
				x1 = robotState.getNavigator().getPoseProvider().getPose().getX();
				y1 = robotState.getNavigator().getPoseProvider().getPose().getY();
				
			}
			// Punktet vi vil hen til
			x2 = robotState.getBallLocation().getX();
			y2 = robotState.getBallLocation().getY();
			
			double deltaX = x2-x1;
			double deltaY = y2-y1;
			
			double heading = Math.toDegrees(Math.atan2(deltaY,deltaX));
			return heading;
		}
	}
}