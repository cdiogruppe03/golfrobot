package behaviors;

import data.RobotState;
import lejos.robotics.navigation.Pose;
import lejos.robotics.subsumption.Behavior;


public class BehaviorCarefulCollection implements Behavior{
	RobotState robotState = RobotState.getInstance();
	
	boolean suppressed = true;
	private double offSet = 5.4;
	private double correctHeadingBorder = 2;
		
	@Override
	public boolean takeControl() {		
		return robotState.isCollectInDangerZone() && robotState.isPoseUptoDate();
	}

	@Override
	public void action() {
		suppressed = false;
		System.out.println("*** BEHAVIOR CAREFULCOLLECTION ***");
		
		if(robotState.isDebug()) {
			System.out.println(robotState);	
		}
		
		Pose robotPose = robotState.getNavigator().getPoseProvider().getPose();

		correctHeading();

		int wiggleSize;
		if(robotState.getBallInCorner()) {
			wiggleSize = 10;
		}
		else {
			wiggleSize = 45;
		}
		
		// Collecting the ball
		double collectingDistance = Math.sqrt(
				Math.pow(robotPose.getX()
						-robotState.getBallLocation().getX(), 2) 
				+ Math.pow(robotPose.getY()
						-robotState.getBallLocation().getY(), 2))
				-robotState.getROBOT_NOSE_LENGTH();
		
		if(collectingDistance > 0) {
			collectingDistance += offSet;
			robotState.getPilot().travel(collectingDistance, false); // False -> Blocking call
			wiggle(wiggleSize);
			robotState.getPilot().travel(-collectingDistance, false);
		} 
		
		robotState.getPilot().setAngularSpeed(100);
		robotState.setPoseUptoDate(false);
		robotState.stateRequestBall();
}

	@Override
	public void suppress() {
		suppressed = true;
	}

	private void wiggle(int wiggleSize) {
		robotState.getPilot().rotate(wiggleSize, false);
		robotState.getPilot().rotate(-2*wiggleSize, false);
		robotState.getPilot().rotate(wiggleSize, false);
	}
	
	/**
	 * Corrects the heading
	 */
	private void correctHeading() {
		// Calculating angle
		double x1, y1, x2, y2;
		x1 = robotState.getNavigator().getPoseProvider().getPose().getX();
		y1 = robotState.getNavigator().getPoseProvider().getPose().getY();
		x2 = robotState.getBallLocation().getX();
		y2 = robotState.getBallLocation().getY();
		
		double deltaX = x2-x1;
		double deltaY = y2-y1;
		
		double heading = Math.toDegrees(Math.atan2(deltaY,deltaX));
		
		if(Math.abs(heading)>correctHeadingBorder)
			robotState.getNavigator().rotateTo(heading);
	}
}
