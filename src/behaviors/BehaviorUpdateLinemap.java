package behaviors;

import data.RobotState;
import lejos.hardware.BrickFinder;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.robotics.geometry.Line;
import lejos.robotics.pathfinding.ShortestPathFinder;
import lejos.robotics.subsumption.Behavior;
import main.RoboRun;

public class BehaviorUpdateLinemap implements Behavior {
	private RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;
	private boolean updatingMap = false;
	
	@Override
	public boolean takeControl() {
		return robotState.isUpdatedLinemap() || updatingMap;
	}

	@Override
	public void action() {
		suppressed = false;
		if(!updatingMap)
			updatingMap = true;
			System.out.println("*** UPDATE LINEMAP BEHAVIOR ***");
			new Thread(new LinemapUpdater()).start();
		
		while(updatingMap && !suppressed)
			Thread.yield();
	}

	@Override
	public void suppress() {
		suppressed = true;
		updatingMap = false;
	}
	
	private class LinemapUpdater implements Runnable{
		private ShortestPathFinder pathFinder;

		@Override
		public void run() {
			try {
				pathFinder = (ShortestPathFinder) robotState.getPathFinder();
				
				if(pathFinder == null) {
					robotState.setPathFinder(new ShortestPathFinder(robotState.getLineMap()));
				}
				else {
					pathFinder.setMap(robotState.getLineMap());
				}
//				((ShortestPathFinder) robotState.getPathFinder()).lengthenLines((float) robotState.getTrackwidth());
				((ShortestPathFinder) robotState.getPathFinder()).lengthenLines(1);
				
				robotState.setHaveLinemap(true);
				robotState.stateRequestBall();
				updatingMap = false;
				
			} catch(NullPointerException e) {
				System.out.println("**** NULLPOINTER EXCEPTION ****");
				System.out.println(robotState);
			}
		}
	}
}
