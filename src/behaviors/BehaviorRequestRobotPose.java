package behaviors;

import java.io.IOException;

import communication.Communicator;
import communication.outgoingMessages;
import data.RobotState;
import lejos.robotics.subsumption.Behavior;

public class BehaviorRequestRobotPose implements Behavior{
	RobotState robotState = RobotState.getInstance();
	private boolean suppressed = true;

	@Override
	public boolean takeControl() {
		return (!robotState.isPoseUptoDate() 
				&& robotState.isHaveLinemap() 
				&& !robotState.isPoseRequested() 
				&& !robotState.isPoseReceived());
	}

	@Override
	public void action() {
		suppressed = false;
		robotState.setPoseRequested(true);
		Communicator communicator;
		System.out.println("*** BEHAVIOR REQUEST ROBOT POSE ***");
		if(robotState.isDebug()) {

			System.out.println(robotState);	
		}
		
		try {
			communicator = Communicator.getInstance();
			communicator.sendData(outgoingMessages.RequestRobotPose);
		} catch (IOException e) {
			System.out.println("ERROR: COULDN'T FETCH COMMUNICATOR INSTANCE.");
			System.exit(0);
		}
	}

	@Override
	public void suppress() {
		suppressed = true;
	}

}
