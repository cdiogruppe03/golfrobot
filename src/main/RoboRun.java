package main;

import behaviors.BehaviorCarefulCollection;
import behaviors.BehaviorCarefulCollectionSensor;
import behaviors.BehaviorDefaultDoNothing;
import behaviors.BehaviorUnloadBalls;
import behaviors.BehaviorEndRun;
import behaviors.BehaviorMoveToBall;
import behaviors.BehaviorMoveToGoal;
import behaviors.BehaviorProcessBallLocation;
import behaviors.BehaviorProcessRobotPose;
import behaviors.BehaviorReadyToStart;
import behaviors.BehaviorRequestBallLocation;
import behaviors.BehaviorRequestRobotPose;
import behaviors.BehaviorUpdateLinemap;
import behaviors.BehaviorWaitForConnection;
import behaviors.BehaviorWaitForCross;
import behaviors.BehaviorWaitForGoalLocation;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class RoboRun {
	public static void main(String[] args) {		
		Behavior[] bArray = {
				new BehaviorDefaultDoNothing(),
				new BehaviorMoveToBall(),
				new BehaviorCarefulCollectionSensor(),
				new BehaviorReadyToStart(),
				new BehaviorRequestBallLocation(),
				new BehaviorRequestRobotPose(),
				new BehaviorProcessBallLocation(),
				new BehaviorProcessRobotPose(),
				new BehaviorWaitForGoalLocation(),
				new BehaviorWaitForCross(),
				new BehaviorUpdateLinemap(),
				new BehaviorWaitForConnection(),
				new BehaviorMoveToGoal(),
				new BehaviorUnloadBalls(),
				new BehaviorEndRun(),
		};
		Arbitrator arb = new Arbitrator(bArray);
		arb.go();
	}
}